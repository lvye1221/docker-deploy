# docker-deploy

#### centos7 下 docker 的安装

```
sudo yum install -y yum-utils device-mapper-persistent-data lvm2

sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install docker-ce -y

```


#### 项目介绍
docker 环境搭建相关代码


git clone https://gitee.com/lvye1221/docker-deploy



【参考资料】docker compose php+mysql+nginx 连载，包含3篇
https://blog.csdn.net/tang05709/article/details/77996246


操作命令：
```

// 安装库依赖
docker-compose build

// 将会在后台启动并运行所有的容器
docker-compose up -d


// 启动服务器
docker-compose up
```

#### mysql

让装好的 mysql 支持远程连接

host为 % 表示不限制ip   localhost表示本机使用    plugin非mysql_native_password 则需要修改密码
```
// 进入 mysql 容器 的命令行
docker exec -it docker_mysql_1 bash


mysql -uroot -p123456

mysql> ALTER user 'root'@'%' IDENTIFIED WITH mysql_native_password BY '123456'; 
Query OK, 0 rows affected (0.03 sec)

mysql> FLUSH PRIVILEGES;  
Query OK, 0 rows affected (0.00 sec)


```

参考网址： docker安装mysql遇到的问题
https://blog.csdn.net/zhaokejin521/article/details/80468908


##### docker 2.0 内存配置

docker_mysql_1 exited with code 137

问题
https://www.petefreitag.com/item/848.cfm


【解决办法】 设置 Linux 交换分区

详细参照此文档：
https://blog.csdn.net/zstack_org/article/details/53258588

```
fallocate -l 2G /swapfile
ls -lh /swapfile
chmod 600 /swapfile
mkswap /swapfile
swapon /swapfile


vi /etc/fstab
/swapfile   swap    swap    sw  0   0




```

ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)


###### 单独测试 mysql

```
docker search mysql

docker pull mysql:5.6

docker images |grep mysql


docker run -p 3306:3306 --name mymysql -v $PWD/conf:/etc/mysql/conf.d -v $PWD/logs:/logs -v $PWD/data:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=123456 -d mysql:5.6


docker exec -it mymysql bash

```



#### 安装vi

apt-get update

apt-get install -y vi 
apt-get install -y vim




No input file specified.

```

<?php
echo phpinfo();

```



docker pull mysql

docker run -d -p 127.0.0.1:3306:3306 –name mysql -v ./testdb:/var/lib/mysql -e MYSQL_ROOT_PASSWORD=”111111” mysql:latest


docker run --name docker_mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=123456 -d docker.io/mysql restart=always  




1 FastCGI sent in stderr: "Unable to open primary script: /code/t.php (Operation not permitted)" 

1 FastCGI sent in stderr: "Unable to open primary script:  (Operation not permitted)" 



web_1    | 2018/08/13 08:28:36 [error] 6#6: *2 FastCGI sent in stderr: "Unable to open primary script: /code/t.php (Operation not permitted)" while reading response header from upstream, client: 183.214.113.54, server: localhost, request: "GET /t.php HTTP/1.1", upstream: "fastcgi://172.19.0.2:9000", host: "hope1995.me:8080"


[error] 6#6: *2 FastCGI sent in stderr: Unable to open primary script:  (Operation not permitted)

【解决办法】删除 htdocs 目录下的 .user.ini 文件


# 连接 mysql 数据库

主机名，是写 docker-compse 中定义的镜像名字 mysql


用户名的权限要给够

docker-compose 密码加密16位改为41位

```

mysql
select host,user,password from mysql.user;
SET PASSWORD FOR 'root'@'%' = PASSWORD('123456'); 


---------
set @@session.old_passwords=0;
FLUSH PRIVILEGES; 

mysql> select password(“123456”); 
+——————————————-+ 
| password(“123456”) | 
+——————————————-+ 
| *6BB4837EB74329105EE4568DDA7DC67ED2CA2AD9 | 
+——————————————-+ 
1 row in set (0.00 sec)

```


[68ae8015] 2018-08-14 01:58:09: Fatal exception of type MWException

chmod 777 images;

